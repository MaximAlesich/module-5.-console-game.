﻿namespace GameNew
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Damage { get; set; }
        public char Char { get; set; }
    }
}