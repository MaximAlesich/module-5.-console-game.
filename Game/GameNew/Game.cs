﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace GameNew
{
    public class Game
    {
        private const int InitHelth = 10;
        private const int MaxDamage = 10;
        private const int MinDamage = 1;

        private List<Point> _points;
        private Random _random;
        private int _health;
        private int _cursorPosLeft;
        private int _cursorPosTop;

        public Game()
        {
            _points = new List<Point>();
            _random = new Random((int)DateTime.Now.Second);
        }

        public void Init()
        {
            _health = InitHelth;
            FillMatrix();
            FillMatrixTrap();
            DrawMatrix();
            DrawScoreBoard();
        }

        public void Start()
        {
            SetStartPoint();
            WeidForAction();
        }

        private void RestartGame()
        {
            Console.Clear();
            _points.Clear();
            Init();
            Start();
        }

        private void FillMatrix()
        {
            for (int row = 0; row < 10; row++)
            {
                for (int column = 0; column < 20; column += 2)
                {
                    _points.Add(new Point() { X = column, Y = row, Char = '*' });
                }
            }
            _points[0].Char = '+';
        }

        private void FillMatrixTrap()
        {
            for (int i = 0; i < 10; i++)
            {
                int randomIndex = _random.Next(1, _points.Count - 1);
                int randomDamage = _random.Next(MinDamage, MaxDamage + 1);

                _points[randomIndex].Damage = randomDamage;
            }
        }

        private void DrawMatrix()
        {
            foreach (Point point in _points)
            {
                Console.SetCursorPosition(point.X, point.Y);
                Console.Write(point.Char);
            }
        }

        private void DrawScoreBoard()
        {
            Console.SetCursorPosition(0, 10);
            Console.WriteLine($"___________________\n");
            Console.WriteLine($"  Осталось здоровья: {_health} из {InitHelth}  \n");
            Console.WriteLine($"  $ - передвигающийся по игровому полю принц\n" +
                              $"  + - принцесса (принц должен спасти её)\n\n" +
                              $"  По мере продвижения (попадая в ловушки) у Вас \n  будут отниматься очки здоровья");
        }

        private void SetStartPoint()
        {
            _cursorPosLeft = _points[_points.Count - 1].X;
            _cursorPosTop = _points[_points.Count - 1].Y;
            Console.SetCursorPosition(_cursorPosLeft, _cursorPosTop);
            Console.Write('$');
            Console.SetCursorPosition(_cursorPosLeft, _cursorPosTop);
        }

        private void WeidForAction()
        {
            ConsoleKeyInfo key = Console.ReadKey(true);
            while (key.Key != ConsoleKey.Escape)
            {
                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        Console.Write('*');
                        _cursorPosTop--;
                        MoveCursor(ref _cursorPosLeft, ref _cursorPosTop);
                        Console.Write('$');
                        MoveCursor(ref _cursorPosLeft, ref _cursorPosTop);
                        ChekDamage();
                        break;

                    case ConsoleKey.DownArrow:
                        Console.Write('*');
                        _cursorPosTop++;
                        MoveCursor(ref _cursorPosLeft, ref _cursorPosTop);
                        Console.Write('$');
                        MoveCursor(ref _cursorPosLeft, ref _cursorPosTop);
                        ChekDamage();
                        break;

                    case ConsoleKey.LeftArrow:
                        Console.Write('*');
                        _cursorPosLeft -= 2;
                        MoveCursor(ref _cursorPosLeft, ref _cursorPosTop);
                        Console.Write('$');
                        MoveCursor(ref _cursorPosLeft, ref _cursorPosTop);
                        ChekDamage();
                        break;

                    case ConsoleKey.RightArrow:
                        Console.Write('*');
                        _cursorPosLeft += 2;
                        MoveCursor(ref _cursorPosLeft, ref _cursorPosTop);
                        Console.Write('$');
                        MoveCursor(ref _cursorPosLeft, ref _cursorPosTop);
                        ChekDamage();
                        break;
                }
                key = Console.ReadKey(true);
            }
        }

        private void ChekDamage()
        {
            for (int i = 0; i < _points.Count; i++)
            {
                if (_points[i].X == _cursorPosLeft && _points[i].Y == _cursorPosTop)
                {
                    if (_points[i].Damage > 0)
                    {
                        _health -= _points[i].Damage;
                        DrawScoreBoard();
                    }


                    if (_health <= 0)
                    {
                        Console.Clear();
                        Console.WriteLine($"Вы проиграли.\nЖелаете попробовать еще раз?\nНажмите Y для начала игры либо N для выхода из игры");
                        Console.Write("");
                        string userChoice = Console.ReadLine();
                        if (userChoice.Equals("Y", StringComparison.OrdinalIgnoreCase))
                        {
                            RestartGame();
                        }

                        if (userChoice.Equals("N", StringComparison.OrdinalIgnoreCase))
                        {
                            Environment.Exit(0);
                        }

                    }

                    if (_points[i].X == 0 && _points[i].Y == 0)
                    {
                        Console.Clear();
                        Console.WriteLine($"Поздравляем!!! Вы победили!!!\nЖелаете попробовать еще раз?\nНажмите Y для начала игры либо N для выхода из игры ");
                        Console.Write("");
                        string userChoice = Console.ReadLine();
                        if (userChoice.Equals("Y", StringComparison.OrdinalIgnoreCase))
                        {
                            RestartGame();
                        }

                        if (userChoice.Equals("N", StringComparison.OrdinalIgnoreCase))
                        {
                            Environment.Exit(0);
                        }
                    }
                }
            }
        }

        private void MoveCursor(ref int left, ref int top)
        {
            if (left > 18)
            {
                left = 18;
            }

            if (top < 0)
            {
                top = 0;
            }

            if (left < 0)
            {
                left = 0;
            }

            if (top > 9)
            {
                top = 9;
            }

            Console.SetCursorPosition(left, top);
        }
    }
}

